					Vaizdų atpažinimo tinklo analitika

    Programa skirta generuoti bei vizualizuoti (analizuoti) Convolution Network (vaizdų atpažinimo Neural-net) 
    parametrus pagal pageidavimą, nusakančius modelio efektyvumą: recall, accuracy, precision, f1 ir kt.
	Aplikacija yra sudaryta iš keturių klasių. Klasių aprašai pagal hierarchiją:
1.App

Ši klasė randasi hierarchijos viršuje. Joje sugeneruojami (convolution) modeliai ir jie yra vizualizuojami. Duomeinms vizualizuoti paimtas org.math.plot.PlotPanel modulis. Jis paima reikiamus modelio duomenis ir gali juos vizualizuoti įvairiais būdais (2d , 3d, scatter, line ir kt.). Iš šios klasės paleidžiamas kodas.

2.Plotter

Ši klasė aprašo vizuallizavimo kodą. Joje panaudojami JFreeChart ir Plot3DPanel moduliai.
3.Creator

Šioje klasėje panaudojant ConvModel klasėje aprašytą funkcionalą sukuriamas convolution network modelis, užtikrinamas maksimalus kuriamo objekto lankstumas. Galime kurti panašias klases su įvairiomis modelio kofigūracijomis.

4.ConvModel

Šioje klasėje yra aprašyti metodai sąsūkio tinklo konstravimui. Panaudota deeplearning4j biblioteka.

5.RangeConvert

Į šią klasę sudėti pagalbiniai metodai generuoti pagalbiniams duomenims (grid'o formavimui), duomenų konvertavimui.
