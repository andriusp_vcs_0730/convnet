package convolution;

import org.deeplearning4j.datasets.iterator.impl.CifarDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.lossfunctions.LossFunctions;

public class Creator {
    
    private final boolean pretrain = true;
    private final Integer nIn = 3;
    private final Integer nOut = 20;
    private final Integer pad = 2;
    private final Integer iters = 5;
    private final Integer kernSize = 5;
    private final Integer kernel = 2;
    private final Integer stride = 1;
    private final Integer nOutOut = 10;
    private final Integer batchSize = 2;
    private final Integer seed = 12345;
    private final Integer conOut0 = 16;
    private final Integer strideSub = 2;
    private final Integer divisorTrain = 1;
    
    private final Double moment = .5;
    private final Double l2 = .001;
    private final Double lr = .03;
    
    private final InputType convInput = InputType.convolutional(32, 32, 3);

    private final Activation RELU = Activation.RELU;
    private final Activation SOFTMAX = Activation.SOFTMAX;
    private final WeightInit XAVIER = WeightInit.XAVIER;
    private final LossFunctions.LossFunction NegLogLL = LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD;
    private final SubsamplingLayer.PoolingType poolingMax = SubsamplingLayer.PoolingType.MAX;
    private final OptimizationAlgorithm optiSGD = OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT;
    private final Updater NESTEROV =  Updater.NESTEROVS;

    private final CifarDataSetIterator dataTest;
    private final CifarDataSetIterator dataTrain;
    private final Evaluation model;
    private final MultiLayerNetwork multiLayNet;
    private final ConvModel convolutionModel;
    private final NeuralNetConfiguration.ListBuilder nnLB;


    public Creator(Integer samples, Double learningRate) {

        convolutionModel = new ConvModel(samples, learningRate);

        dataTrain = convolutionModel.createDataSetIterator(batchSize, divisorTrain, pretrain);

        ConvolutionLayer conv0 = convolutionModel.createConLay(kernSize, nIn, conOut0, stride, pad, XAVIER,
                "con0", RELU);
        SubsamplingLayer sub0 = convolutionModel.createSubLay(poolingMax, kernel, strideSub, "sub0");
        ConvolutionLayer conv1 = convolutionModel.createConLay(kernSize, nOut, stride, pad, XAVIER, "con1", RELU);
        SubsamplingLayer sub1 = convolutionModel.createSubLay(poolingMax, kernel, strideSub, "sub1");
        ConvolutionLayer conv2 = convolutionModel.createConLay(kernSize, nOut, stride, pad, XAVIER, "con2", RELU);
        SubsamplingLayer sub2 = convolutionModel.createSubLay(poolingMax, kernel, strideSub, "sub2");

        OutputLayer outLayer = convolutionModel.createOutLay(NegLogLL, SOFTMAX, XAVIER, "output", nOutOut);

        nnLB = convolutionModel.createNeuralNetConfigListBuilder(seed, iters,
                optiSGD, lr, true, l2, NESTEROV, moment);

        convolutionModel.addLayer(nnLB, conv0);
        convolutionModel.addLayer(nnLB, sub0);
        convolutionModel.addLayer(nnLB, conv1);
        convolutionModel.addLayer(nnLB, sub1);
        convolutionModel.addLayer(nnLB, conv2);
        convolutionModel.addLayer(nnLB, sub2);
        convolutionModel.addLayer(nnLB, outLayer);

        MultiLayerConfiguration mlconf = convolutionModel.createMultiLayerConfig(nnLB, pretrain, true, convInput);

        Integer divisorTest = 4;
        dataTest = convolutionModel.createDataSetIterator(batchSize, divisorTest, true);

        multiLayNet = convolutionModel.createMultiLayerNetwork(mlconf, dataTrain);

        this.model = convolutionModel.createModel(multiLayNet, dataTest);

    }

        public Evaluation getModel () {
        return this.model;
    }
}
