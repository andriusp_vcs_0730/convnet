package convolution;

import org.apache.log4j.BasicConfigurator;
import org.deeplearning4j.datasets.iterator.impl.CifarDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.lossfunctions.LossFunctions;


public class ConvModel {

    private final Integer samples;
    private final Double learningRate;

    private int cnt;

    public ConvModel(Integer samples, Double learningRate) {

        this.samples = samples;
        this.learningRate = learningRate;
        this.cnt = 0;

    }

    public CifarDataSetIterator createDataSetIterator (Integer batchSize, Integer divisor, boolean train) {

        return new CifarDataSetIterator(batchSize, this.samples/divisor, train);
    }

    public ConvolutionLayer createConLay (Integer kernel, Integer nIn, Integer nOut, Integer stride, Integer pad,
                                         WeightInit weight, String name, Activation act) {

        return new ConvolutionLayer.Builder(kernel, kernel).nIn(nIn).nOut(nOut).stride(stride, stride).padding(pad, pad)
                .weightInit(weight).name(name).activation(act).build();

    }

    public ConvolutionLayer createConLay (Integer kernel, Integer nOut, Integer stride, Integer pad,
                                          WeightInit weight, String name, Activation act) {

        return new ConvolutionLayer.Builder(kernel, kernel).nOut(nOut).stride(stride, stride).padding(pad, pad)
                .weightInit(weight).name(name).activation(act).build();

    }

    public SubsamplingLayer createSubLay (SubsamplingLayer.PoolingType pool, Integer kernSize, Integer stride,
                                          String name) {

        return new SubsamplingLayer.Builder(pool).kernelSize(kernSize, kernSize).stride(stride, stride)
                .name("First subsmpl layer").build();
    }

    public OutputLayer createOutLay (LossFunctions.LossFunction lossF, Activation activ, WeightInit weight, String name,
                                     Integer nOut) {

        return new OutputLayer.Builder(lossF).activation(activ).weightInit(weight).name(name).nOut(nOut).build();
    }

    public NeuralNetConfiguration.ListBuilder createNeuralNetConfigListBuilder (Integer seed, Integer iters, OptimizationAlgorithm opti,
                                                                                Double lr, boolean regul, Double l2, Updater updater,
                                                                                Double moment) {

        NeuralNetConfiguration.ListBuilder nn = new NeuralNetConfiguration.Builder()
                .seed(seed).iterations(iters).optimizationAlgo(opti)
                .learningRate(lr).regularization(regul).l2(lr).updater(updater)
                .momentum(moment).list();

        return nn;
    }

    public void addLayer (NeuralNetConfiguration.ListBuilder nn, ConvolutionLayer con) {
        nn.layer(cnt, con);
        cnt += 1;
    }
    public void addLayer (NeuralNetConfiguration.ListBuilder nn, SubsamplingLayer sub) {
        nn.layer(cnt, sub);
        cnt += 1;
    }
    public void addLayer (NeuralNetConfiguration.ListBuilder nn, OutputLayer out) {
        nn.layer(cnt, out);
        cnt += 1;
    }

    MultiLayerConfiguration createMultiLayerConfig (NeuralNetConfiguration.ListBuilder nn, boolean pretr,
                                                    boolean backprop, InputType intype) {
        return nn.pretrain(pretr).backprop(backprop)
                .setInputType(intype).build();

    }

    MultiLayerNetwork createMultiLayerNetwork (MultiLayerConfiguration conf, CifarDataSetIterator cds) {
        MultiLayerNetwork mln = new MultiLayerNetwork(conf);
        mln.init();
        mln.fit(cds); // training of network
        return mln;
    }

    public Evaluation createModel(MultiLayerNetwork mln, CifarDataSetIterator dataTrain) {

        BasicConfigurator.configure(); // dingsta WARN bekompiliuojant

        return mln.evaluate(dataTrain); // evaluating model performance
    }

}