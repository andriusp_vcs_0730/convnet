package convolution;

import org.math.plot.Plot3DPanel;

import javax.swing.JFrame;
import java.awt.Color;

public class Plotter {

    public final Plot3DPanel plot3d;
    private JFrame frame;
    private final int width = 700;

    public Plotter (double[] param1, double[] param2, double[] param3) {

        plot3d = new Plot3DPanel();
        plot3d.addLinePlot("recall versus precision versus accurat",
                Color.BLUE, param1, param2, param3);

    }

    public void setAxLabel (Integer axe, String label) {
        plot3d.setAxisLabel(axe, label);
    }

    public void showPlot() {
        frame = new JFrame("a plot panel");
        frame.setContentPane(plot3d);
        frame.setSize(width, width);
        frame.setVisible(true);
    }
}
