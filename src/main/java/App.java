import convolution.Creator;
import convolution.Plotter;
import convolution.RangeConvert;
import org.deeplearning4j.eval.Evaluation;

import java.util.List;

import static org.junit.Assert.*;

public class App {

    private static App instance = null;

    private final Integer start = 80;
    private final Integer stop = 190;
    private final Integer step = 55;

    private final Double startD = .1;
    private final Double stopD = .3;
    private final Double stepD = .1;

    private final Double learnRate = .01;

    private final double[] modelAcc;
    private final double[] modelF1;
    private final double[] modelPrec;

    private final List<Integer> samples;

    private final RangeConvert ran;

    protected Evaluation evaler;
    private Plotter plot;

    private App() {

        ran = new RangeConvert();

        this.samples = ran.range(start, stop, step);

        modelAcc = new double[samples.size()];
        modelF1 = new double[samples.size()];
        modelPrec = new double[samples.size()];

        for (Integer i = 0; i < samples.size(); i += 1) {

            evaler = new Creator(samples.get(i), learnRate).getModel();

            //evaler = model.getModel();

            modelAcc[i] = evaler.accuracy();
            modelF1[i] = evaler.f1();
            modelPrec[i] = evaler.precision();
        }
//
//        plot = new Plotter(modelAcc, modelPrec, modelF1);
//
//        plot.setAxLabel(0, "accuracy");
//        plot.setAxLabel(1, "precision");
//        plot.setAxLabel(2, "f1");
//
//        plot.showPlot();
    }

    public static synchronized App getInstance() {
        if (instance == null) {
            instance = new App();
        }
        return instance;
    }


    public static void main(String[] args) {

        App app = App.getInstance();

        assertEquals(app.evaler.accuracy(), app.modelPrec[app.modelPrec.length - 1], .0001);
        assertNotNull(getInstance());
        assertSame(getInstance(), app);
        assertTrue(app instanceof App);

        double value;

        try {
            value = app.modelPrec[10];
            fail("Wrong index value");
        }
        catch (NullPointerException e) {
            value = app.modelPrec[app.modelPrec.length - 1];
            e.getMessage();
        }
    }
}
